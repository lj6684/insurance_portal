-- MySQL dump 10.13  Distrib 5.5.38, for Win32 (x86)
--
-- Host: localhost    Database: ia
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addr` varchar(200) DEFAULT NULL,
  `post_code` varchar(20) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about`
--

LOCK TABLES `about` WRITE;
/*!40000 ALTER TABLE `about` DISABLE KEYS */;
INSERT INTO `about` VALUES (1,'吉林省长春市经开区昆山路1195号','130031','0431-81928611'),(2,NULL,NULL,NULL);
/*!40000 ALTER TABLE `about` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `create_time` datetime NOT NULL,
  `content` text NOT NULL,
  `clicks` int(11) NOT NULL DEFAULT '0',
  `come_from` varchar(100) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `cid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'协会简介','2015-11-12 11:13:48','<p>内容测试</p>',0,'吉林','张三2',2),(2,'协会近况','2015-11-12 11:14:45','<p>this is my test</p>',0,'吉林','李四',1),(3,'测试通知','2015-11-12 11:15:37','<p>测试内容</p>',0,'新疆','王五',2),(4,'动态1','2015-11-12 12:24:01','<p>abc</p>',0,'吉林省保险行业协会','吉林省保险行业协会',3),(5,'动态2','2015-11-12 12:24:35','<p>abc</p>',0,'吉林省保险行业协会','吉林省保险行业协会',3);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
INSERT INTO `catalog` VALUES (1,'协会概况'),(2,'通知公告'),(3,'协会动态'),(4,'行业资讯'),(5,'政策法规'),(6,'保险知识'),(7,'经典案例'),(8,'办事指南');
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `download`
--

DROP TABLE IF EXISTS `download`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `file` varchar(200) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `download`
--

LOCK TABLES `download` WRITE;
/*!40000 ALTER TABLE `download` DISABLE KEYS */;
INSERT INTO `download` VALUES (1,'报名表','经销商销售管理系统需求规格说明书.doc','2015-11-07 22:21:54'),(2,'T2','经销商销售管理系统需求规格说明书1.doc','2015-11-07 22:23:42'),(4,'T333','经销商销售管理系统需求规格说明书.doc','2015-11-07 23:09:24');
/*!40000 ALTER TABLE `download` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `content` text NOT NULL,
  `create_time` datetime NOT NULL,
  `tel` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,'张三','this is test message 中文测试','2015-11-09 15:41:03','13894841305'),(2,'李四','this is test2','2015-11-09 16:56:08','13588447744'),(3,'T3','TEST333','2015-11-09 17:46:28','13488774744');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `office`
--

DROP TABLE IF EXISTS `office`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `office` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `addr` varchar(100) NOT NULL,
  `post_code` varchar(10) NOT NULL,
  `tel` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `office`
--

LOCK TABLES `office` WRITE;
/*!40000 ALTER TABLE `office` DISABLE KEYS */;
INSERT INTO `office` VALUES (1,'白山办事处','<p><span style=\"WIDOWS: 1; TEXT-TRANSFORM: none; TEXT-INDENT: 0px; DISPLAY: inline !important; FONT: 12px/24px Simsun; WHITE-SPACE: normal; FLOAT: none; LETTER-SPACING: 1px; COLOR: rgb(51,51,51); WORD-SPACING: 0px; -webkit-text-stroke-width: 0px\">&nbsp;&nbsp;&nbsp; 吉林省保险行业协会白城办事处成立于2005年10月8日，是吉林省保险行业协会的派出机构，接受中国保监会吉林监管局依法进行管理、指导和监督。办事处自成立以来，在吉林保监局、吉林省保险行业协会的正确指导和全体会员单位的共同努力下，坚持以服务为宗旨，以市场为导向，以发展为主题，以改革为动力，以会员需求为目标，充分发挥“自律、维权、服务、交流”的职能作用，在行业自律、外部协调、信息交流、受理投诉、宣传服务、调查研究、团结协作以及维护权益等方面作了大量的工作，对完善监管体系，规范市场秩序，创造公平竞争环境，促进业务发展等方面起到了不可代替的作用；成为同业间相互学习、交流、合作的平台，成为保险经营者与政府沟通的桥梁和宣传的窗口。</span><br style=\"WIDOWS: 1; TEXT-TRANSFORM: none; TEXT-INDENT: 0px; FONT: 12px/24px Simsun; WHITE-SPACE: normal; LETTER-SPACING: 1px; COLOR: rgb(51,51,51); WORD-SPACING: 0px; -webkit-text-stroke-width: 0px\"/><span style=\"WIDOWS: 1; TEXT-TRANSFORM: none; TEXT-INDENT: 0px; DISPLAY: inline !important; FONT: 12px/24px Simsun; WHITE-SPACE: normal; FLOAT: none; LETTER-SPACING: 1px; COLOR: rgb(51,51,51); WORD-SPACING: 0px; -webkit-text-stroke-width: 0px\">　　截至目前，协会白城办事处共有市级会员单位16家，其中财险公司8家、寿险公司8家；县（区）机构51家。自律组织有白城市产险工作委员会、寿险工作委员会2个专业工作委员会，各工作委员会的日常工作由办事处承担。协会白城办事处编制5人，目前有员工4人，设主任1人，办事机构由综合部、财险部、中介部、寿险部和电子化考试中心组成。</span><br style=\"WIDOWS: 1; TEXT-TRANSFORM: none; TEXT-INDENT: 0px; FONT: 12px/24px Simsun; WHITE-SPACE: normal; LETTER-SPACING: 1px; COLOR: rgb(51,51,51); WORD-SPACING: 0px; -webkit-text-stroke-width: 0px\"/><span style=\"WIDOWS: 1; TEXT-TRANSFORM: none; TEXT-INDENT: 0px; DISPLAY: inline !important; FONT: 12px/24px Simsun; WHITE-SPACE: normal; FLOAT: none; LETTER-SPACING: 1px; COLOR: rgb(51,51,51); WORD-SPACING: 0px; -webkit-text-stroke-width: 0px\">　　协会白城办事处的宗旨是：遵守国家宪法、法律、法规和经济金融方针政策，坚持以邓小平理论和“三个代表”重要思想为指导，深入贯彻落实科学发展观，依据《中华人民共和国保险法》，在国家对保险业实行集合统一监督管理的前提下，配合保险监管部门督促会员自律，维护行业利益，促进行业发展，为会员提供服务，促进市场公开、公平、公正，全面提高保险服务社会主义和谐社会的能力。</span></p>','白城市幸福南大街47号','137000','0436-3242653');
/*!40000 ALTER TABLE `office` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `org`
--

DROP TABLE IF EXISTS `org`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `org` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `org`
--

LOCK TABLES `org` WRITE;
/*!40000 ALTER TABLE `org` DISABLE KEYS */;
INSERT INTO `org` VALUES (1,1,'人保财险','20120517161826914.jpg','<p>TEST111</p>'),(2,2,'太平洋寿险','201205171618269141.jpg','<p>TEST222</p>');
/*!40000 ALTER TABLE `org` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orgtype`
--

DROP TABLE IF EXISTS `orgtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orgtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orgtype`
--

LOCK TABLES `orgtype` WRITE;
/*!40000 ALTER TABLE `orgtype` DISABLE KEYS */;
INSERT INTO `orgtype` VALUES (1,'产险公司会员单位'),(2,'寿险公司会员单位'),(3,'中介结构会员单位');
/*!40000 ALTER TABLE `orgtype` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-12 13:42:31
