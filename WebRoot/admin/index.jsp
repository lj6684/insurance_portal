<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>门户后台管理</title>
    <link href="../css/admin_login.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="admin_login_wrap">
    <h1>后台管理</h1>
    <div class="adming_login_border">
        <div class="admin_input">
            <div>${notice }</div>
            <form action="/admin/login" method="post">
                <ul class="admin_items">
                    <li>
                        <label for="user">管理员账号：</label>
                        <input type="text" name="username" id="username" size="40" class="admin_input_style" />
                    </li>
                    <li>
                        <label for="pwd">管理员密码：</label>
                        <input type="password" name="password" id="pwd" size="40" class="admin_input_style" />
                    </li>
                    <li>
                        <input type="submit" tabindex="3" value="提交" class="btn btn-primary" />
                    </li>
                </ul>
            </form>
        </div>
    </div>
    <p class="admin_copyright"><a tabindex="5" href="http://www.mycodes.net/" target="_blank">返回首页</a></p>
</div>
</body>
</html>