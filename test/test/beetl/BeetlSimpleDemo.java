package test.beetl;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.core.resource.StringTemplateResourceLoader;

/**
 * Created by ipc on 15/10/27.
 */
public class BeetlSimpleDemo {

    public void stringResourceLoader() {
        try {
            StringTemplateResourceLoader resourceLoader = new StringTemplateResourceLoader();
            Configuration cfg = Configuration.defaultConfiguration();
            GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);

            Template t = gt.getTemplate("hello,${name}");
            t.binding("name", "beetl");
            String str = t.render();
            System.out.println(str);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void classpathResourceLoader() {
        try {
            ClasspathResourceLoader resourceLoader = new ClasspathResourceLoader();
            Configuration cfg = Configuration.defaultConfiguration();
            GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);

            Template t = gt.getTemplate("/test/beetl/hello.txt");
            t.binding("name", "beetl");
            String str = t.render();
            System.out.println(str);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        BeetlSimpleDemo demo = new BeetlSimpleDemo();
        //ia.stringResourceLoader();
        demo.classpathResourceLoader();
    }
}
