package test.beetl;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ipc on 15/10/27.
 */
public class UserListDemo {

    public void doTest() {
        try {
            ClasspathResourceLoader cpLoader = new ClasspathResourceLoader();
            Configuration config = Configuration.defaultConfiguration();
            GroupTemplate groupTemplate = new GroupTemplate(cpLoader, config);

            Template t = groupTemplate.getTemplate("/test/beetl/userlist.txt");
            List<User> userList = new ArrayList<User>();
            userList.add(new User("Tom", 20));
            userList.add(new User("Jack", 22));
            userList.add(new User("Merry", 20));

            t.binding("userList", userList);

            String outString = t.render();
            System.out.printf(outString);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        UserListDemo demo = new UserListDemo();
        demo.doTest();
    }
}
