package com.ia.util;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-12
 * Time: 上午9:18
 * To change this template use File | Settings | File Templates.
 */
public class ShowMessage {

    public static final String ADD_SUCCESS = "添加成功";
    public static final String UPDATE_SUCCESS = "更新成功";
    public static final String DELETE_SUCCESS = "删除成功";
    public static final String ERROR = "操作失败";
    public static final String UPLOAD_FILE_NULL = "获取上传文件失败(file=null)";

}
