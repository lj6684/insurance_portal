package com.ia.config;

import com.ia.admin.activity.ActivityController;
import com.ia.admin.article.Article;
import com.ia.admin.article.ArticleController;
import com.ia.admin.download.Download;
import com.ia.admin.about.About;
import com.ia.admin.about.AboutController;
import com.ia.admin.catalog.CatalogController;
import com.ia.admin.download.DownloadController;
import com.ia.admin.message.MessageController;
import com.ia.admin.notice.NoticeController;
import com.ia.admin.office.OfficeController;
import com.ia.admin.org.Org;
import com.ia.admin.org.OrgController;
import com.ia.admin.orgtype.OrgType;
import com.ia.index.IndexController;
import com.ia.admin.catalog.Catalog;
import com.ia.admin.message.Message;
import com.ia.admin.office.Office;
import com.ia.util.Path;
import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import org.beetl.core.GroupTemplate;
import org.beetl.ext.jfinal.BeetlRenderFactory;


/**
 * API引导式配置
 */
public class MainConfig extends JFinalConfig {
	
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		loadPropertyFile("config.txt");

		me.setMainRenderFactory(new BeetlRenderFactory());

		GroupTemplate gt = BeetlRenderFactory.groupTemplate;
		// 可以通过gt设置全局共享变量

		me.setDevMode(PropKit.getBoolean("devMode", false));

        try {
            me.setUploadedFileSaveDirectory(Path.APP_PATH + "upload");
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add("/", IndexController.class, "/index");	// 第三个参数为该Controller的视图存放路径

		me.add("/admin/article", ArticleController.class);
		me.add("/admin/catalog", CatalogController.class);
		me.add("/admin/office", OfficeController.class);
		me.add("/admin/message", MessageController.class);
		me.add("/admin/about", AboutController.class);
        me.add("/admin/download", DownloadController.class);
        me.add("/admin/org", OrgController.class);
        me.add("/admin/notice", NoticeController.class);
        me.add("/admin/activity", ActivityController.class);
    }
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
		C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password").trim());
		me.add(c3p0Plugin);
		
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		me.add(arp);
		arp.addMapping("article", Article.class);
		arp.addMapping("catalog", Catalog.class);
		arp.addMapping("office", Office.class);
		arp.addMapping("message", Message.class);
		arp.addMapping("about", About.class);
		arp.addMapping("download", Download.class);
		arp.addMapping("org", Org.class);
		arp.addMapping("orgtype", OrgType.class);
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		
	}
	
	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		JFinal.start("WebRoot", 8080, "/", 5);
	}
}
