package com.ia.cache;

import com.ia.admin.orgtype.OrgType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-11
 * Time: 上午9:02
 * To change this template use File | Settings | File Templates.
 */
public class OrgTypeCache extends OptionCache {

    private static OrgTypeCache instance;

    public static OrgTypeCache getInstance() {
        if(instance == null) {
            synchronized (OrgTypeCache.class) {
                if(instance == null) {
                    instance = new OrgTypeCache();
                }
            }
        }
        return instance;
    }

    private OrgTypeCache() {
        refresh();
    }

    public void refresh() {
        List<OrgType> orgTypeList = OrgType.me.find("select * from orgtype order by id");
        list = new ArrayList<Option>();
        for(OrgType orgType : orgTypeList) {
            list.add(new Option(orgType.getStr("name"), orgType.getInt("id")));
        }
    }
}
