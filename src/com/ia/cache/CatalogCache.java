package com.ia.cache;

import com.ia.admin.catalog.Catalog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-11
 * Time: 上午9:23
 * To change this template use File | Settings | File Templates.
 */
public class CatalogCache extends OptionCache {
    private static CatalogCache instance;

    public static CatalogCache getInstance() {
        if(instance == null) {
            synchronized (CatalogCache.class) {
                if(instance == null) {
                    instance = new CatalogCache();
                }
            }
        }
        return instance;
    }

    private CatalogCache() {
        refresh();
    }

    @Override
    public void refresh() {
        List<Catalog> catalogList = Catalog.me.find("select * from catalog order by id");
        list = new ArrayList<Option>();
        for(Catalog catalog : catalogList) {
            list.add(new Option(catalog.getStr("name"), catalog.getInt("id")));
        }
    }
}
