package com.ia.cache;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-11
 * Time: 上午9:07
 * To change this template use File | Settings | File Templates.
 */
public class Option {
    private String name;
    private int value;

    public Option(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
