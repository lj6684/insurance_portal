package com.ia.cache;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-11
 * Time: 下午2:28
 * To change this template use File | Settings | File Templates.
 */
public abstract class OptionCache {
    protected List<Option> list;

    public abstract void refresh();

    public List<Option> getOptionList() {
        return list;
    }

    public List<Option> getOptionListAll() {
        List<Option> options = new ArrayList<Option>();
        options.add(new Option("全部", -1));
        options.addAll(list);

        return options;
    }

    public String getNameByValue(int value) {
        for(Option option : list) {
            if(option.getValue() == value) {
                return option.getName();
            }
        }
        return "";
    }

    public int getValueByName(String name) {
        for(Option option : list) {
            if(option.getName().equals(name)) {
                return option.getValue();
            }
        }
        return -1;
    }
}
