package com.ia.admin.catalog;

import com.jfinal.plugin.activerecord.Model;

/**
 +-------+-------------+------+-----+---------+----------------+
 | Field | Type        | Null | Key | Default | Extra          |
 +-------+-------------+------+-----+---------+----------------+
 | id    | int(11)     | NO   | PRI | NULL    | auto_increment |
 | name  | varchar(50) | NO   |     | NULL    |                |
 +-------+-------------+------+-----+---------+----------------+
 */
public class Catalog extends Model<Catalog> {

    public static final Catalog me = new Catalog();

}
