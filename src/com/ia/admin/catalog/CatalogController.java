package com.ia.admin.catalog;

import com.ia.util.ShowMessage;
import com.jfinal.core.Controller;

import java.util.List;

/**
 * Created by ipc on 15/10/28.
 */
public class CatalogController extends Controller {

    public void index() {
        List<Catalog> catalogList = Catalog.me.find("select * from catalog order by id");
        setAttr("catalogList", catalogList);

        render("index.html");
    }

    public void save() {
        getModel(Catalog.class).save();
        setAttr("showMessage", ShowMessage.ADD_SUCCESS);
        index();
    }

    public void edit() {
        setAttr("catalog", Catalog.me.findById(getParaToInt("id")));

        render("edit.html");
    }

    public void update() {
        getModel(Catalog.class).update();
        setAttr("showMessage", ShowMessage.UPDATE_SUCCESS);
        index();
    }

    public void delete() {
        Catalog.me.deleteById(getParaToInt("id"));
        setAttr("showMessage", ShowMessage.DELETE_SUCCESS);
        index();
    }
}
