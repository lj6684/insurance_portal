package com.ia.admin.message;

import com.ia.util.ShowMessage;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

import java.util.Date;
import java.util.List;

/**
 * Created by ipc on 15/10/28.
 */
public class MessageController extends Controller {

    public void index() {
        int currentPage = getParaToInt(0, 1);
        Page<Message> messagePage = Message.me.paginate(currentPage, 10, "select *", "from message order by create_time desc");
        setAttr("messagePage", messagePage);
        render("index.html");
    }

    public void add() {
        render("add.html");
    }

    public void save() {
        Message message = getModel(Message.class);
        message.set("create_time", new Date());
        message.save();

        setAttr("showMessage", ShowMessage.ADD_SUCCESS);
        index();
    }

    public void delete() {
        Message.me.deleteById(getParaToInt("id"));
        setAttr("showMessage", ShowMessage.DELETE_SUCCESS);
        index();
    }

    public void view() {
        setAttr("message", Message.me.findById(getPara("id")));
        render("view.html");
    }
}
