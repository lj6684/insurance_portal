package com.ia.admin.message;

import com.jfinal.plugin.activerecord.Model;

/**
 +-------------+-------------+------+-----+---------+----------------+
 | Field       | Type        | Null | Key | Default | Extra          |
 +-------------+-------------+------+-----+---------+----------------+
 | id          | int(11)     | NO   | PRI | NULL    | auto_increment |
 | name        | varchar(30) | NO   |     | NULL    |                |
 | article     | text        | NO   |     | NULL    |                |
 | create_time | datetime    | NO   |     | NULL    |                |
 | tel         | varchar(20) | YES  |     | NULL    |                |
 +-------------+-------------+------+-----+---------+----------------+
 */
public class Message extends Model<Message> {

    public static final Message me = new Message();

}
