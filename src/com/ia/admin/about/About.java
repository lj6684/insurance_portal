package com.ia.admin.about;

import com.jfinal.plugin.activerecord.Model;

/**
 +-----------+--------------+------+-----+---------+----------------+
 | Field     | Type         | Null | Key | Default | Extra          |
 +-----------+--------------+------+-----+---------+----------------+
 | id        | int(11)      | NO   | PRI | NULL    | auto_increment |
 | addr      | varchar(200) | YES  |     | NULL    |                |
 | post_code | varchar(20)  | YES  |     | NULL    |                |
 | tel       | varchar(20)  | YES  |     | NULL    |                |
 +-----------+--------------+------+-----+---------+----------------+
 *
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-7
 * Time: 下午12:37
 * To change this template use File | Settings | File Templates.
 */
public class About extends Model<About> {

    public static final About me = new About();

}
