package com.ia.admin.about;

import com.ia.util.ShowMessage;
import com.jfinal.core.Controller;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-7
 * Time: 下午12:42
 * To change this template use File | Settings | File Templates.
 */
public class AboutController extends Controller {

    public void index() {
        About about = About.me.findById(1);
        setAttr("about", about);
        render("index.html");
    }

    public void save() {
        getModel(About.class).update();
        setAttr("showMessage", ShowMessage.UPDATE_SUCCESS);
        index();
    }
}
