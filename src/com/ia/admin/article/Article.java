package com.ia.admin.article;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

import java.util.ArrayList;
import java.util.List;

/**
 +-------------+--------------+------+-----+---------+----------------+
 | Field       | Type         | Null | Key | Default | Extra          |
 +-------------+--------------+------+-----+---------+----------------+
 | id          | int(11)      | NO   | PRI | NULL    | auto_increment |
 | title       | varchar(200) | NO   |     | NULL    |                |
 | create_time | datetime     | NO   |     | NULL    |                |
 | article     | text         | NO   |     | NULL    |                |
 | clicks      | int(11)      | NO   |     | 0       |                |
 | come_from   | varchar(100) | YES  |     | NULL    |                |
 | author      | varchar(30)  | YES  |     | NULL    |                |
 | catalog_id  | int(11)      | NO   |     | NULL    |                |
 +-------------+--------------+------+-----+---------+----------------+
 *
 */
public class Article extends Model<Article> {

    public static final Article me = new Article();

    public Page<Article> search(int pageNumber, int pageSize, int cid, String title) {
        String where = "from article where 1=1";
        List<Object> params = new ArrayList<Object>();
        if(cid > 0) {
            where += " and cid=?";
            params.add(cid);
        }
        if(title != null && !title.equals("")) {
            where += " and title like ?";
            params.add("%" + title + "%");
        }
        where += " order by id desc";

        return paginate(pageNumber, pageSize, "select *", where, params.toArray());
    }

}
