package com.ia.admin.orgtype;

import com.jfinal.plugin.activerecord.Model;

/**
 +-------+-------------+------+-----+---------+----------------+
 | Field | Type        | Null | Key | Default | Extra          |
 +-------+-------------+------+-----+---------+----------------+
 | id    | int(11)     | NO   | PRI | NULL    | auto_increment |
 | name  | varchar(50) | YES  |     | NULL    |                |
 +-------+-------------+------+-----+---------+----------------+
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-11
 * Time: 下午2:37
 * To change this template use File | Settings | File Templates.
 */
public class OrgType extends Model<OrgType> {
    public static final OrgType me = new OrgType();
}
