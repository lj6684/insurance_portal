package com.ia.admin.notice;

import com.ia.admin.article.Article;
import com.ia.cache.CatalogCache;
import com.ia.util.ShowMessage;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-12
 * Time: 上午11:19
 * To change this template use File | Settings | File Templates.
 */
public class NoticeController extends Controller {

    public void index() {
        int cid = CatalogCache.getInstance().getValueByName("通知公告");
        String title = getPara("title", "");

        int fromPage = getParaToInt("from", 1);
        Page<Article> articlePage = Article.me.search(fromPage, 10, cid, title);
        setAttr("noticePage", articlePage);

        setAttr("cid", cid);
        setAttr("title", title);
        setAttr("urlParas", "?title=" + title);

        render("index.html");
    }

    public void add() {
        int cid = CatalogCache.getInstance().getValueByName("通知公告");
        setAttr("cid", cid);
        render("add.html");
    }

    public void save() {
        Article article = getModel(Article.class);
        article.set("create_time", new Date());
        article.save();

        setAttr("showMessage", ShowMessage.ADD_SUCCESS);

        index();
    }

    public void edit() {
        int cid = CatalogCache.getInstance().getValueByName("通知公告");
        setAttr("cid", cid);
        setAttr("article", Article.me.findById(getPara("id")));

        render("edit.html");
    }

    public void update() {
        getModel(Article.class).update();
        setAttr("showMessage", ShowMessage.UPDATE_SUCCESS);

        index();
    }

    public void delete() {
        Article.me.deleteById(getPara("id"));
        setAttr("showMessage", ShowMessage.DELETE_SUCCESS);

        index();
    }

    public void view() {
        setAttr("notice", Article.me.findById(getPara("id")));
        render("view.html");
    }

}
