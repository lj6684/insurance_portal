package com.ia.admin.download;

import com.jfinal.plugin.activerecord.Model;

/**
 +-------------+--------------+------+-----+---------+----------------+
 | Field       | Type         | Null | Key | Default | Extra          |
 +-------------+--------------+------+-----+---------+----------------+
 | id          | int(11)      | NO   | PRI | NULL    | auto_increment |
 | name        | varchar(200) | YES  |     | NULL    |                |
 | file        | varchar(200) | YES  |     | NULL    |                |
 | create_time | datetime     | NO   |     | NULL    |                |
 +-------------+--------------+------+-----+---------+----------------+
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-7
 * Time: 下午12:50
 * To change this template use File | Settings | File Templates.
 */
public class Download extends Model<Download> {
    public static final Download me = new Download();
}
