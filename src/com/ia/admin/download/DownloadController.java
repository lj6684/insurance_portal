package com.ia.admin.download;

import com.ia.util.ShowMessage;
import com.jfinal.core.Controller;
import com.jfinal.upload.UploadFile;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-7
 * Time: 下午12:50
 * To change this template use File | Settings | File Templates.
 */
public class DownloadController extends Controller {

    public void index() {
        List<Download> downloadList = Download.me.find("select * from download order by create_time desc");
        setAttr("downloadList", downloadList);
        render("index.html");
    }

    public void add() {
        render("add.html");
    }

    public void save() {
        UploadFile file = null;
        String showMessage = null;
        try {
            file = getFile("file", "download");
            String dir = file.getSaveDirectory();
            if(file == null) {
                throw new Exception(ShowMessage.UPLOAD_FILE_NULL);
            }

            Download download = new Download();
            download.set("name", getPara("name"));
            download.set("file", file.getFileName());
            download.set("create_time", new Date());
            download.save();

            showMessage = ShowMessage.ADD_SUCCESS;
        } catch (Exception ex) {
            showMessage = ShowMessage.ERROR +":" + ex.toString();
        }
        setAttr("showMessage", showMessage);
        index();
    }

    public void delete() {
        Download.me.deleteById(getParaToInt("id"));
        setAttr("showMessage", ShowMessage.DELETE_SUCCESS);
        index();
    }
}
