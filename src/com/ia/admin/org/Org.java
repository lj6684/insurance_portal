package com.ia.admin.org;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import net.sf.cglib.reflect.MethodDelegate;

import java.util.ArrayList;
import java.util.List;

/**
 * +---------+--------------+------+-----+---------+----------------+
 * | Field   | Type         | Null | Key | Default | Extra          |
 * +---------+--------------+------+-----+---------+----------------+
 * | id      | int(11)      | NO   | PRI | NULL    | auto_increment |
 * | type    | int(11)      | NO   |     | NULL    |                |
 * | name    | varchar(100) | NO   |     | NULL    |                |
 * | logo    | varchar(100) | YES  |     | NULL    |                |
 * | article | text         | YES  |     | NULL    |                |
 * +---------+--------------+------+-----+---------+----------------+
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-7
 * Time: 下午1:56
 * To change this template use File | Settings | File Templates.
 */
public class Org extends Model<Org> {
    public static final Org me = new Org();

    public Page<Org> search(int pageNumber, int pageSize, int type, String name) {
        String where = "from org where 1=1";
        List<Object> params = new ArrayList<Object>();
        if(type > 0) {
            where += " and type=?";
            params.add(type);
        }
        if(name != null && !name.equals("")) {
            where += " and name like ?";
            params.add("%" + name + "%");
        }
        where += " order by id desc";

        return paginate(pageNumber, pageSize, "select *", where, params.toArray());
    }
}
