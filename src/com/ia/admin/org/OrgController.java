package com.ia.admin.org;

import com.ia.cache.OrgTypeCache;
import com.ia.util.ShowMessage;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 15-11-7
 * Time: 下午1:56
 * To change this template use File | Settings | File Templates.
 */
public class OrgController extends Controller {

    public void index() {
        int type = getParaToInt("type", -1);
        String orgName = getPara("orgName", "");

        int fromPage = getParaToInt("from", 1);
        Page<Org> orgPage = Org.me.search(fromPage, 10, type, orgName);
        setAttr("orgPage", orgPage);

        setAttr("type", type);
        setAttr("orgName", orgName);
        setAttr("urlParas", "?type=" + type + "&orgName=" + orgName);
        setAttr("orgTypeCache", OrgTypeCache.getInstance());

        render("index.html");
    }

    public void add() {;
        setAttr("orgTypeCache", OrgTypeCache.getInstance());
        render("add.html");
    }

    public void save() {
        UploadFile file = null;
        String showMessage = null;
        try {
            file = getFile("logo", "logo");
            if(file == null) {
                throw new Exception(ShowMessage.UPLOAD_FILE_NULL);
            }
            Org org = getModel(Org.class);
            org.set("logo", file.getFileName());
            org.save();

            showMessage = ShowMessage.ADD_SUCCESS;
        } catch (Exception ex) {
            showMessage = ShowMessage.ERROR + ":" + ex.toString();
        }
        setAttr("showMessage", showMessage);
        index();
    }

    public void edit() {
        setAttr("org", Org.me.findById(getPara("id")));
        setAttr("orgTypeCache", OrgTypeCache.getInstance());
        render("edit.html");
    }

    public void update() {
        UploadFile file = null;
        String showMessage = null;
        try {
            file = getFile("logo", "logo");
            String changeLogo = getPara("changeLogo");
            if(changeLogo.equalsIgnoreCase("true")) {
                // 更换LOGO
                if(file == null) {
                    throw new Exception(ShowMessage.UPLOAD_FILE_NULL);
                }
                Org org = getModel(Org.class);
                org.set("logo", file.getFileName());
                org.update();
            } else {
                // 不更换LOGO
                getModel(Org.class).update();
            }
            showMessage = ShowMessage.UPDATE_SUCCESS;
        } catch (Exception ex) {
            showMessage = ShowMessage.ERROR + ":" + ex.toString();
        }
        setAttr("showMessage", showMessage);
        index();
    }

    public void delete() {
        Org.me.deleteById(getPara("id"));
        setAttr("showMessage", ShowMessage.DELETE_SUCCESS);
        index();
    }

    public void view() {
        setAttr("org", Org.me.findById(getPara("id")));
        setAttr("orgTypeCache", OrgTypeCache.getInstance());
        render("view.html");
    }
}
