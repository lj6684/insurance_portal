package com.ia.admin.office;

import com.ia.util.ShowMessage;
import com.jfinal.core.Controller;

import java.util.List;

/**
 * Created by ipc on 15/10/28.
 */
public class OfficeController extends Controller {

    public void index() {
        List<Office> officeList = Office.me.find("select * from office order by id");
        setAttr("officeList", officeList);

        render("index.html");
    }

    public void add() {
        render("add.html");
    }

    public void view() {
        setAttr("office", Office.me.findById(getParaToInt("id")));
        render("view.html");
    }

    public void save() {
        Office office = getModel(Office.class);
        office.save();
        setAttr("showMessage", ShowMessage.ADD_SUCCESS);
        index();
    }

    public void edit() {
        setAttr("office", Office.me.findById(getParaToInt("id")));
        render("edit.html");
    }

    public void update() {
        getModel(Office.class).update();
        setAttr("showMessage", ShowMessage.UPDATE_SUCCESS);
        index();
    }

    public void delete() {
        Office.me.deleteById(getParaToInt("id"));
        setAttr("showMessage", ShowMessage.DELETE_SUCCESS);
        index();
    }
}
