package com.ia.admin.office;

import com.jfinal.plugin.activerecord.Model;

/**
 +-----------+--------------+------+-----+---------+----------------+
 | Field     | Type         | Null | Key | Default | Extra          |
 +-----------+--------------+------+-----+---------+----------------+
 | id        | int(11)      | NO   | PRI | NULL    | auto_increment |
 | name      | varchar(200) | NO   |     | NULL    |                |
 | article   | text         | NO   |     | NULL    |                |
 | addr      | varchar(100) | NO   |     | NULL    |                |
 | post_code | varchar(10)  | NO   |     | NULL    |                |
 | tel       | varchar(30)  | NO   |     | NULL    |                |
 +-----------+--------------+------+-----+---------+----------------+
 */
public class Office extends Model<Office> {

    public static final Office me = new Office();
}
